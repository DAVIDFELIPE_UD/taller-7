function iniciarJuego() {
    let i;
  
    let divTablero = document.getElementById("divTablero");
    
    for (i = 1; i < 65; i++) {
      let unaCasilla = document.createElement("input");
      unaCasilla.type = "button";
      unaCasilla.value = " ";
      unaCasilla.setAttribute("id", "casilla" + i);
      unaCasilla.setAttribute("class", "casilla");
      unaCasilla.setAttribute("onClick", "realizarJugada(this.id)");
      divTablero.appendChild(unaCasilla);
      if (i % 8 === 0) {
        let salto = document.createElement("br");
        divTablero.appendChild(salto);
      }
    }
  }
  